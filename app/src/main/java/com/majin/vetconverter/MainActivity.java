package com.majin.vetconverter;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.majin.vetconverter.advanced.AdvancedActivity;
import com.majin.vetconverter.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.getRoot().setVisibility(View.VISIBLE);
        binding.splashImage.setVisibility(View.VISIBLE);
        binding.splashImage.bringToFront();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                startNextActivity();
            }
        }, 2250);

    }

    private void startNextActivity() {
        startActivity(new Intent(this, AdvancedActivity.class));
    }
}
