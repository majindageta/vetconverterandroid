package com.majin.vetconverter.utils;

import java.util.Locale;

/**
 * Created by davidericci on 23/01/18.
 */

public class Converter {
    public enum MISURING_UNIT {
        ml,
        cl,
        dl,
        mg,
        cg,
        dg,
        l,
        g,
        percentage
    }

    public enum PICKER_SELECTED{
        principle,
        total,
        target
    }

    static public String[] misuringUnitSolution = {MISURING_UNIT.ml.toString(),  MISURING_UNIT.cl.toString(),  MISURING_UNIT.dl.toString(), MISURING_UNIT.l.toString(), MISURING_UNIT.mg.toString(), MISURING_UNIT.cg.toString(), MISURING_UNIT.dg.toString(), MISURING_UNIT.g.toString(), "percentage"};
    static public String[] misuringUnitTotal = {MISURING_UNIT.ml.toString(),  MISURING_UNIT.cl.toString(),  MISURING_UNIT.dl.toString(), MISURING_UNIT.l.toString(), MISURING_UNIT.mg.toString(), MISURING_UNIT.cg.toString(), MISURING_UNIT.dg.toString(), MISURING_UNIT.g.toString()};

    private static String actualMisuringUnitSolution = MISURING_UNIT.ml.toString();    //1
    private static String actualMisuringUnitTotal = MISURING_UNIT.ml.toString();        //2
    private static String actualMisuringUnitTarget = MISURING_UNIT.ml.toString();      //3
    public static String actualMisuringUnitResult = MISURING_UNIT.ml.toString();      //4

    public static double makeConvertion (double activePrinciples, double total, double target, double weight) {

        if (actualMisuringUnitSolution.equals("percentage")) {
            //percentage calculation: activePrinciples is a percentage from 1 to 100
            //need to calculate first the value of the actual percentage then use it to do proportion for target

            //New fantastic calculation (considera sempre mg/ml oppure g/L
            // % è principio attivo / 100 g o ml quindi 9% soluzione ho 9 grammi in 100ml
            double actualTarget  = target;
            if (actualMisuringUnitTarget.equals(MISURING_UNIT.mg.toString()) || actualMisuringUnitTarget.equals(MISURING_UNIT.ml.toString())) {
                actualTarget = target / 1000;
            }
            if (actualMisuringUnitTarget.equals(MISURING_UNIT.cg.toString()) || actualMisuringUnitTarget.equals(MISURING_UNIT.cl.toString())) {
                actualTarget = target / 100;
            }
            if (actualMisuringUnitTarget.equals(MISURING_UNIT.dg.toString()) || actualMisuringUnitTarget.equals(MISURING_UNIT.dl.toString())) {
                actualTarget = target / 10;
            }

            double fantasticCalculation = actualTarget / activePrinciples * 100;
            return weight != 0 ? fantasticCalculation * weight : fantasticCalculation;
        } else {
            return weight != 0 ? (target/activePrinciples) * (total) * weight : (target/activePrinciples) * (total);
        }
    }

    public static double returnMultiplier(double unit, int item) {
        double value  = unit;
        if (value != -1) {
            if (actualMisuringUnitResult.equals(MISURING_UNIT.ml.toString()) ||
                    actualMisuringUnitResult.equals(MISURING_UNIT.mg.toString())) {
                if (misuringUnitSolution[item].equals(MISURING_UNIT.cl.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.cg.toString())) {
                    value = value / 10;
                }
                if (misuringUnitSolution[item].equals(MISURING_UNIT.dl.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.dg.toString())) {
                    value = value / 100;
                }
                if (misuringUnitSolution[item].equals(MISURING_UNIT.l.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.g.toString())) {
                    value = value / 1000;
                }
            } else if (actualMisuringUnitResult.equals(MISURING_UNIT.cl.toString()) ||
                    actualMisuringUnitResult.equals(MISURING_UNIT.cg.toString())) {
                if (misuringUnitSolution[item].equals(MISURING_UNIT.ml.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.mg.toString())) {
                    value = value * 10;
                }
                if (misuringUnitSolution[item].equals(MISURING_UNIT.dl.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.dg.toString())) {
                    value = value / 10;
                }
                if (misuringUnitSolution[item].equals(MISURING_UNIT.l.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.g.toString())) {
                    value = value / 100;
                }
            } else if (actualMisuringUnitResult.equals(MISURING_UNIT.dl.toString()) ||
                    actualMisuringUnitResult.equals(MISURING_UNIT.dg.toString())) {

                if (misuringUnitSolution[item].equals(MISURING_UNIT.ml.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.mg.toString())) {
                    value = value * 100;
                }
                if (misuringUnitSolution[item].equals(MISURING_UNIT.cl.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.cg.toString())) {
                    value = value * 10;
                }
                if (misuringUnitSolution[item].equals(MISURING_UNIT.l.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.g.toString())) {
                    value = value / 10;
                }
            } else if (actualMisuringUnitResult.equals(MISURING_UNIT.l.toString()) ||
                    actualMisuringUnitResult.equals(MISURING_UNIT.g.toString())) {
                if (misuringUnitSolution[item].equals(MISURING_UNIT.ml.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.mg.toString())) {
                    value = value * 1000;
                }
                if (misuringUnitSolution[item].equals(MISURING_UNIT.cl.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.cg.toString())) {
                    value = value * 100;
                }
                if (misuringUnitSolution[item].equals(MISURING_UNIT.dl.toString()) ||
                        misuringUnitSolution[item].equals(MISURING_UNIT.dg.toString())) {
                    value = value * 10;
                }
            }
        }
        return value;
    }

    public static String formatDecimal(double value, boolean weight)  {
        if (weight)  return String.format(Locale.getDefault(),"%.2f", value);
        return String.format(Locale.getDefault(),"%.3f", value);
    }

   public static double revertFormatDecimalToDouble(String value) {
        try {
            //TODO: check for Locale error
            if (value.contains(",")) value = value.replace(",",".");
            return Double.parseDouble(value);
        }
        catch (Exception e) {
            return -1;
        }

    }

    public static void selectUnitSolution (String value, int actualUnitIndex) {

        for (String s : misuringUnitSolution) {
            if (s.equals(value)) {

                switch (actualUnitIndex) {
                    case 1:
                        actualMisuringUnitSolution = MISURING_UNIT.valueOf(value).toString();
                        break;
                    case 2:
                        actualMisuringUnitTotal = MISURING_UNIT.valueOf(value).toString();
                        break;
                    case 3:
                        actualMisuringUnitTarget = MISURING_UNIT.valueOf(value).toString();
                        break;
                    case 4:
                        actualMisuringUnitResult = MISURING_UNIT.valueOf(value).toString();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
