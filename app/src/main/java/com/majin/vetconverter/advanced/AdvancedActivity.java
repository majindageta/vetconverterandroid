package com.majin.vetconverter.advanced;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.majin.vetconverter.R;
import com.majin.vetconverter.databinding.ActivityAdvancedBinding;

/**
 * Created by davidericci on 12/01/18.
 */

public class AdvancedActivity extends AppCompatActivity {

    private ActivityAdvancedBinding binding;
    private AdvancedViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_advanced);
        viewModel = new AdvancedViewModel(this, binding);
        binding.setViewModel(viewModel);

        viewModel.createComboConverterItems();
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public void calculateButtonPressed(View v) {
        hideKeyboard();
        viewModel.calculateButtonPressed();
    }



}
