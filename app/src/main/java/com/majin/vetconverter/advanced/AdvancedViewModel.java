package com.majin.vetconverter.advanced;

import android.content.Context;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.majin.vetconverter.R;
import com.majin.vetconverter.comboConverter.ComboConverter;
import com.majin.vetconverter.comboConverter.ComboConverterInterface;
import com.majin.vetconverter.databinding.ActivityAdvancedBinding;
import com.majin.vetconverter.utils.Converter;

import java.util.Locale;

/**
 * Created by davidericci on 23/01/18.
 */

public class AdvancedViewModel extends BaseObservable {


    private Context context;
    private ActivityAdvancedBinding binding;
    private ComboConverter comboConverterSolution;
    private ComboConverter comboConverterTarget;
    private ComboConverter comboConverterTotal;
    private ComboConverter comboConverterResult;
    private int totalComboIndex;

    AdvancedViewModel(@NonNull Context context, ActivityAdvancedBinding binding) {
        this.context = context;
        this.binding = binding;
    }

    void createComboConverterItems() {
        comboConverterSolution = new ComboConverter(context, binding.solutionCombo.rv, binding.solutionCombo.button,
                getData(true), 1, new ComboConverterInterface() {
            @Override
            public void onButtonPressed(int comboIndex) {
                buttonPressed(comboIndex);
            }

            @Override
            public void onRecyclerViewItemSelected(int comboIndex, int index) {
                recyclerViewSelectedItem(comboIndex, index);
            }
        });
        comboConverterSolution.setUpView();

        comboConverterTotal = new ComboConverter(context, binding.totalCombo.rv, binding.totalCombo.button,
                getData(false), 2, new ComboConverterInterface() {
            @Override
            public void onButtonPressed(int comboIndex) {
                buttonPressed(comboIndex);
            }

            @Override
            public void onRecyclerViewItemSelected(int comboIndex, int index) {
                recyclerViewSelectedItem(comboIndex, index);
            }
        });
        comboConverterTotal.setUpView();

        comboConverterTarget = new ComboConverter(context, binding.targetCombo.rv, binding.targetCombo.button,
                getData(false), 3, new ComboConverterInterface() {
            @Override
            public void onButtonPressed(int comboIndex) {
                buttonPressed(comboIndex);
            }

            @Override
            public void onRecyclerViewItemSelected(int comboIndex, int index) {
                recyclerViewSelectedItem(comboIndex, index);
            }
        });
        comboConverterTarget.setUpView();

        comboConverterResult = new ComboConverter(context, binding.resultCombo.rv, binding.resultCombo.button,
                getData(false), 4, new ComboConverterInterface() {
            @Override
            public void onButtonPressed(int comboIndex) {
                buttonPressed(comboIndex);
            }

            @Override
            public void onRecyclerViewItemSelected(int comboIndex, int index) {
                recyclerViewSelectedItem(comboIndex, index);
            }
        });
        comboConverterResult.setUpView();

        setUpUI();
    }

    private void setUpUI() {
        binding.targetCombo.secondRow.setVisibility(View.GONE);
        binding.resultCombo.firstRow.setVisibility(View.GONE);

        binding.solutionCombo.textView.setText("Principio attivo");
        binding.totalCombo.textView.setText("Quantità totale");
        binding.targetCombo.textView.setText("Principio necessario");
        binding.resultCombo.textView.setText("");

        binding.result.setVisibility(View.GONE);
        binding.resultHelp.setVisibility(View.GONE);
        binding.resultCombo.secondRow.setVisibility(View.GONE);

        binding.solutionCombo.rv.setSelectedItem(4);
        binding.totalCombo.rv.setSelectedItem(0);
        binding.resultCombo.rv.setSelectedItem(4);
        binding.targetCombo.rv.setSelectedItem(7);
    }


    private CharSequence[] getData(boolean percentageEnabled) {
        Resources res = context.getResources();
        CharSequence[] misuring;
        if (percentageEnabled) {
            misuring = res.getStringArray(R.array.solution_datasource);
        } else {
            misuring = res.getStringArray(R.array.total_datasource);
        }

        return  misuring;
    }

    private void recyclerViewSelectedItem(int comboIndex, int item) {
        ((AdvancedActivity)context).hideKeyboard();

        if (comboIndex == 1) {
            Log.d("SOLUTION", item + "");
            Converter.selectUnitSolution(Converter.misuringUnitSolution[item],  1);
            if (Converter.misuringUnitSolution[item].equals("percentage")) {
                binding.targetCombo.secondRow.setVisibility(View.VISIBLE);
                binding.targetCombo.rv.setSelectedItem(7);
                binding.resultCombo.rv.setSelectedItem(7);

                binding.totalCombo.editText.setText("100");
                binding.totalCombo.editText.setEnabled(false);
                binding.totalCombo.rv.setEnabled(false);
                binding.totalCombo.button.setEnabled(false);
            } else {
                binding.targetCombo.secondRow.setVisibility(View.GONE);
                binding.targetCombo.rv.setSelectedItem(4);
                binding.resultCombo.rv.setSelectedItem(4);

                binding.totalCombo.editText.setEnabled(true);
                binding.totalCombo.rv.setEnabled(true);
                binding.totalCombo.button.setEnabled(true);
            }
        }

        if (comboIndex == 2) {
            Log.d("TOTAL", item + "");
            Converter.selectUnitSolution(Converter.misuringUnitSolution[item],  2);
            if (!Converter.misuringUnitSolution[item].equals("%")) {
                Converter.selectUnitSolution( Converter.misuringUnitSolution[item], 4);
                binding.resultCombo.rv.setSelectedItem(item);
            }
            totalComboIndex = item;
        }

        if (comboIndex == 3) {
            Log.d("TARGET", item + "");
            Converter.selectUnitSolution(Converter.misuringUnitTotal[item], 3);
        }

        if (comboIndex == 4) {
            Log.d("RESULT", item + "");
            double value = Converter.revertFormatDecimalToDouble(binding.result.getText().toString());
            if (value != -1) {
                value = Converter.returnMultiplier(value, item);
                binding.result.setText(Converter.formatDecimal(value, false));
                String resultHelpString = binding.resultHelp.getText().toString();
                if (resultHelpString.length() > 4) {
                    int range = resultHelpString.indexOf(Converter.actualMisuringUnitResult + ":");

                    if (range > 0) {
                        String substring = resultHelpString.substring(0, range);
                        resultHelpString = substring + Converter.misuringUnitSolution[item] + ":";
                        binding.resultHelp.setText(resultHelpString);
                    }
                }
            }
            Converter.selectUnitSolution(Converter.misuringUnitSolution[item], 4);
        }
    }

    void calculateButtonPressed() {
        binding.resultCombo.rv.setSelectedItem(totalComboIndex);
        try {
            Double activePrinciplesDouble = Double.parseDouble(binding.solutionCombo.editText.getText().toString());
            Double totalDouble = Double.parseDouble(binding.totalCombo.editText.getText().toString());
            Double targetDouble = Double.parseDouble(binding.targetCombo.editText.getText().toString());
            Double weightDouble = Double.parseDouble(binding.weightEditText.getText().toString());

            if (activePrinciplesDouble != 0 && totalDouble != 0 && targetDouble != 0) {
                binding.resultCombo.secondRow.setVisibility(View.VISIBLE);

                binding.result.setVisibility(View.VISIBLE);

                binding.resultHelp.setVisibility(View.VISIBLE);
                binding.resultHelp.setText(setUpLocalizedStringForResult(weightDouble));

                binding.result.setText(Converter.formatDecimal(Converter.makeConvertion(activePrinciplesDouble, totalDouble, targetDouble, weightDouble), false));
            } else {
                binding.resultHelp.setText("Hai dimenticato qualche campo!");
            }
        } catch (Exception e) {
            binding.resultHelp.setText("Hai dimenticato qualche campo? altrimenti è un bug. Chiamami!");
        }
    }

    private void buttonPressed(int comboIndex) {
        try {
            switch (comboIndex) {
                case 1:
                    binding.solutionCombo.editText.requestFocus();
                    break;
                case 2:
                    binding.totalCombo.editText.requestFocus();
                    break;
                case 3:
                    binding.targetCombo.editText.requestFocus();
                    break;
                case 4:
                    binding.resultCombo.editText.requestFocus();
                    break;
                default:
                    break;
            }
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

    private String setUpLocalizedStringForResult(double weightDouble) {
        if (Converter.misuringUnitSolution[binding.solutionCombo.rv.getSelectedItem()].equalsIgnoreCase("percentage")) {

            if (weightDouble >= 0) {
                return String.format(Locale.getDefault(),"La soluzione al %1$s%% di principio attivo su %2$s%3$s totali, per arrivare a %4$s%5$s al kg per un totale di %6$s kg, è di %7$s:",
                        binding.solutionCombo.editText.getText(), binding.totalCombo.editText.getText(), Converter.misuringUnitSolution[binding.totalCombo.rv.getSelectedItem()],
                        binding.targetCombo.editText.getText(), Converter.misuringUnitSolution[binding.targetCombo.rv.getSelectedItem()],
                        Converter.formatDecimal(weightDouble, true), Converter.misuringUnitSolution[binding.resultCombo.rv.getSelectedItem()]);
            } else {
                return String.format(Locale.getDefault(),"La soluzione al %1$s%% di principio attivo su %2$s%3$s totali, per arrivare a %4$s%5$s è di %6$s:",
                        binding.solutionCombo.editText.getText(), binding.totalCombo.editText.getText(), Converter.misuringUnitSolution[binding.totalCombo.rv.getSelectedItem()],
                        binding.targetCombo.editText.getText(), Converter.misuringUnitSolution[binding.targetCombo.rv.getSelectedItem()],
                        Converter.misuringUnitSolution[binding.resultCombo.rv.getSelectedItem()]);
            }
        } else {
            if (weightDouble >= 0) {
                return String.format(Locale.getDefault(),"%1$s%2$s di principio attivo su %3$s%4$s totali.\nPer avere %5$s%6$s al kg per un totale di %7$s kg, sono necessari %8$s:",
                        binding.solutionCombo.editText.getText(), Converter.misuringUnitSolution[binding.solutionCombo.rv.getSelectedItem()],
                        binding.totalCombo.editText.getText(), Converter.misuringUnitSolution[binding.totalCombo.rv.getSelectedItem()],
                        binding.targetCombo.editText.getText(), Converter.misuringUnitSolution[binding.targetCombo.rv.getSelectedItem()],
                        Converter.formatDecimal(weightDouble,true), Converter.misuringUnitSolution[binding.resultCombo.rv.getSelectedItem()]);
            } else {
                return String.format(Locale.getDefault(),"%1$s%2$s di principio attivo su %3$s%4$s totali.\nPer avere %5$s%6$s sono necessari %7$s:",
                        binding.solutionCombo.editText.getText(), Converter.misuringUnitSolution[binding.solutionCombo.rv.getSelectedItem()],
                        binding.totalCombo.editText.getText(), Converter.misuringUnitSolution[binding.totalCombo.rv.getSelectedItem()],
                        binding.targetCombo.editText.getText(), Converter.misuringUnitSolution[binding.targetCombo.rv.getSelectedItem()],
                        Converter.misuringUnitSolution[binding.resultCombo.rv.getSelectedItem()]);
            }
        }
    }
}
