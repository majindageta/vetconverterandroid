package com.majin.vetconverter.comboConverter;

/**
 * Created by davidericci on 23/01/18.
 */

public interface ComboConverterInterface {

    void onButtonPressed(int comboIndex);
    void onRecyclerViewItemSelected(int comboIndex, int itemIndex);
}
