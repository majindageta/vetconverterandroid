package com.majin.vetconverter.comboConverter;

import android.content.Context;
import android.view.View;
import android.widget.Button;

import com.wefika.horizontalpicker.HorizontalPicker;

/**
 * Created by davidericci on 23/01/18.
 */

public class ComboConverter implements HorizontalPicker.OnItemSelected, HorizontalPicker.OnItemClicked{

    private Context context;
    private HorizontalPicker horizontalPicker;
    private Button button;
    private CharSequence[] data;
    private int comboIndex;
    private ComboConverterInterface comboConverterInterface;

    public ComboConverter(Context context, HorizontalPicker horizontalPicker, Button button, CharSequence[] data, int comboIndex, ComboConverterInterface comboConverterInterface) {
        this.context = context;
        this.horizontalPicker = horizontalPicker;
        this.button = button;
        this.data = data;
        this.comboIndex = comboIndex;
        this.comboConverterInterface = comboConverterInterface;
    }

    public void setUpView() {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comboConverterInterface.onButtonPressed(comboIndex);
            }
        });

        horizontalPicker.setValues(data);
        horizontalPicker.setOnItemClickedListener(this);
        horizontalPicker.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemClicked(int index) {
        comboConverterInterface.onButtonPressed(index);
    }

    @Override
    public void onItemSelected(int index) {
        comboConverterInterface.onRecyclerViewItemSelected(comboIndex, index);
    }
}
